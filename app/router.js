'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const {
    router,
    controller,
  } = app;
  router.all('/health', function (ctx) {
    ctx.body = 'I am OK !';
  });
  router.get('/*', controller.home.index);
};