const cpuNums = require('os').cpus().length;
const isLocal = process.env.NODE_ENV !== 'production' && !process.env.EGG_SERVER_ENV;

const app = require('egg').startCluster({
  baseDir: __dirname,
  port: process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
  hostname: process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0',
  workers: isLocal ? 1 : Math.min(cpuNums, 10)
});